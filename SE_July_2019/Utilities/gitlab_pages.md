# Gitlab Pages
GitLab Pages is a feature that allows you to publish static websites directly from a repository in GitLab.

You can use it either for personal or business websites, such as portfolios, documentation, manifestos, and business presentations. You can also attribute any license to your content.

To publish a website with Pages, you can use any Static Site Generator (SSG), such as Jekyll, Hugo, Middleman, Harp, Hexo, and Brunch, just to name a few. You can also publish any website written directly in plain HTML, CSS, and JavaScript.


## How to Setup
This tutorial assumes already have a gitlab repo(project) with an `index.html` file pushed ot the repo.
* Add a `.gitlab-ci.yml` to your projects root folder
  * This file tells gitlab that you want it to publish/host your website, for this tutorial we will be focusing on publishing a simple website, but for a deeper dive into the `.gitlab-ci.yml` file [check out this link](https://hackernoon.com/configuring-gitlab-ci-yml-150a98e9765d)
  * Example `.yml` contents for hosting
  ``` yaml
  pages:
  stage: deploy
  script:
  - mkdir .public
  - cp -r * .public
  - mv .public public
  artifacts:
    paths:
    - public
  only:
  - master
  ```
* Alternatively you can have gitlab generate this file
    * To use this method go to your project on gitlab
    * Click on the `Set up CI/CD` Button
    * Select your template, for simple pages we will select the HTML template
    * Commit the file
    * [Video example](https://media.giphy.com/media/Ze9jQfRmduLHrNCatd/giphy.gif)
* Wait for the repo to deploy, then go to Settings->Pages to find the gitlab pages link to your website.
    * Visit the link to ensure it is being hosted, and working properly
    * It can take up to 10-12 minutes for the gitlab pages link to fully post the website and not show a 404 error.


Once the website is being hosted you can submit the link you received in settings.


## Common Errors

### 404
Ensure the root folder contains a file named `index.html`, the website only knows to look for a file specifically named `index` to start the website on.

