# SE July 2019 Demo Notes
[**Git**ing started with Javascript - 7/30/19](./Universal/git_July_7-30-19)

[Avengers Assemble the **Array**s - 8/6/19](./Demos/arrays_and_loops_8-6-19.md)

[Basic HTML Entities - 8/12/19](./Demos/html_entities_8-12-19.md)

[HTML 5 Semantics - 8/13/19](./Demos/semantics_8-13-19.md)

[Nodes, DOM, and Trees - 8/26/19](./Demos/dom_trees_events_8-26-19.md)