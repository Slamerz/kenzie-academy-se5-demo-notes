# *Git*ing started with Javascript - 7/30/19

## What was covered
### Git
[**What is Git?**](https://www.atlassian.com/git/tutorials/what-is-git)\
[Git Basics](https://rubygarage.org/blog/most-basic-git-commands-with-examples)

### Git status
Displays paths that have differences between the index file and the current HEAD commit, paths that have differences between the working tree and the index file, and paths in the working tree that are not tracked by Git (and are not ignored by gitignore). The first are what you would commit by running git commit; the second and third are what you could commit by running git add before running git commit.\
[documentation](https://git-scm.com/docs/git-status)\
[inspecting a git repo](https://www.atlassian.com/git/tutorials/inspecting-a-repository)\
[Understanding git status](https://www.cs.swarthmore.edu/~adanner/help/git/git-status.php)

### Git add
This command updates the index using the current content found in the working tree, to prepare the content staged for the next commit. It typically adds the current content of existing paths as a whole, but with some options it can also be used to add content with only part of the changes made to the working tree files applied, or remove paths that do not exist in the working tree anymore.\
[documentation](https://git-scm.com/docs/git-add)\
[Difference between “git add -A” and “git add .”](https://stackoverflow.com/questions/572549/difference-between-git-add-a-and-git-add)

### Git commit
Create a new commit containing the current contents of the index and the given log message describing the changes. The new commit is a direct child of HEAD, usually the tip of the current branch, and the branch is updated to point to it (unless no branch is associated with the working tree, in which case HEAD is "detached").\
[documentation](https://git-scm.com/docs/git-commit)\
[What are commits?](https://www.atlassian.com/git/tutorials/saving-changes/git-commit)\
[A bit more on commits](https://www.git-tower.com/learn/git/commands/git-commit)

### Git gitignore
A gitignore file specifies intentionally untracked files that Git should ignore. Files already tracked by Git are not affected;\
[documentation](https://git-scm.com/docs/gitignore)\
[saving changes- gitignore](https://www.atlassian.com/git/tutorials/saving-changes/gitignore)\
[gitignore templates](https://github.com/github/gitignore)\
[tutorial](https://guide.freecodecamp.org/git/gitignore/)

### Git remote
Manage the set of repositories ("remotes") whose branches you track.\
[documentation](https://git-scm.com/docs/git-remote)\
[Working with remotes](https://git-scm.com/book/en/v2/Git-Basics-Working-with-Remotes)\
[git syncing](https://www.atlassian.com/git/tutorials/syncing)

### Other
[how to exit vim editor](https://itsfoss.com/how-to-exit-vim/)

[How to make visual studio code my default git editor](https://stackoverflow.com/questions/30024353/how-to-use-visual-studio-code-as-default-editor-for-git)


## Javascript 
[Expressions and Operators](http://www.lib.ru/JAVA/javascr/expr.html)\
[console.log()](https://www.w3schools.com/jsref/met_console_log.asp)
### Chrome Dev Tools
[Chrome Dev Tools Console](https://developers.google.com/web/tools/chrome-devtools/console/)\
[How to run javascript in chrome terminal](https://developers.google.com/web/tools/chrome-devtools/console/javascript)

### Variables
[Variables and Datatypes in JavaScript](https://www.geeksforgeeks.org/variables-datatypes-javascript/)
#### Let
The let statement declares a block scope local variable, optionally initializing it to a value.\
[MDN documentation](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/let)\
[W3 documenation](https://www.w3schools.com/js/js_let.asp)\
[Difference between let and var](https://codeburst.io/difference-between-let-and-var-in-javascript-537410b2d707)\
[Declaring JavaScript Variables: var, let and const](https://scotch.io/courses/10-need-to-know-javascript-concepts/declaring-javascript-variables-var-let-and-const)
#### Datatypes
[Full List](https://javascript.info/types)\
[Numbers](https://www.w3schools.com/js/js_numbers.asp)\
[String](https://www.w3schools.com/jsref/jsref_obj_string.asp)
## HTML 
### Script tag
The `<script>` tag is used to define a client-side script (JavaScript).

The `<script>` element either contains script statements, or it points to an external script file through the src attribute.

Common uses for JavaScript are image manipulation, form validation, and dynamic changes of content.\
[W3 documentation](https://www.w3schools.com/html/html_scripts.asp)\
[Script Javascript tutorial](https://www.w3schools.com/js/default.asp)\
[import javascript file into html](https://www.w3schools.com/tags/att_script_src.asp)\
[How do I link a JavaScript file to a HTML file?](https://stackoverflow.com/questions/13739568/how-do-i-link-a-javascript-file-to-a-html-file)\
[Script Src Tutorial](https://guide.freecodecamp.org/html/attributes/script-src-attribute/)






## Useful resources
[Git Terminal Cheatsheet](https://github.github.com/training-kit/downloads/github-git-cheat-sheet.pdf)\
[Interactive Git Learning Tools](https://try.github.io/)\
[Official Visual studio keyboard cheatsheet](https://code.visualstudio.com/shortcuts/keyboard-shortcuts-windows.pdf)\
[Vscode cheatsheet](https://devhints.io/vscode)