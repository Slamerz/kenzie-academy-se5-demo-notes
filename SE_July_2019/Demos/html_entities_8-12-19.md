# HTML Entities - 8/12/19
HTML entities are a way to display non keyboard characters onto your web page, as well as symbols would usually not display, such as double spaces

[documentation](https://www.w3schools.com/html/html_entities.asp)\
[A list of html entities](https://dev.w3.org/html5/html-author/charref)


``` html
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatibale" content="ie-edge">
<title>Document</title>
</head>
<body>
<button id="killIronMan">Kill Iron Man</button>

<p></p>
&amp;#x1F634;

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed bibendum felis dui, ut sodales velit iaculis non. Duis efficitur eros lorem, eu lobortis leo dignissim in. Morbi porttitor fringilla lectus, sit amet dictum enim malesuada a.

<script src="script.js"></script>
Copyright &copy; 2019
</body>
</html>
```


