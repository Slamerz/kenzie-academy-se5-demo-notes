# Avengers Assemble the Arrays - 8/6/19
## Javascript
useful links for topics covered in this 
[**var** vs **let** vs **const**](https://tylermcginnis.com/var-let-const/)
To start we want to console log each member of the Avengers, we can do this by creating multiple variables to store each Avenger
``` javascript
let avenger1 = "Captain America"
let avenger2 = "Iron Man"
let avenger3 = "Thor"
let avenger4 = "Hulk"
let avenger5 = "Black Widow"
let avenger6 = "Hawkeye"
console.log(avenger1 + " ")
console.log(avenger2 + " ")
console.log(avenger3 + " ")
console.log(avenger4 + " ")
console.log(avenger5 + " ")
console.log(avenger6 + " ")
```

This gets a little tedious and can be hard to work with, but luckily this can be changed to an **array** to make the data easier to view and work with.\
So using an **array** the code can now looks something like this.

``` javascript
let avengers = ["Captain America", "Iron Man", "Thor", "Hulk", "Black Widow", "Hawkeye"]
console.log(avengers)
```

[You can use brackets to and the index number of the element in the array](https://www.w3schools.com/js/js_arrays.asp) to access and display single avengers\
so to display just captain america the code would look something like.\
[How to Add Elements to an Array](https://www.dyn-web.com/javascript/arrays/add.php)
``` javascript
let avengers = ["Captain America", "Iron Man", "Thor", "Hulk", "Black Widow", "Hawkeye"]
console.log(avengers[0])
```

You can also add to this array after we've created it by assigning using either an empty index, or a `.push`.
``` javascript
let avengers = ["Captain America", "Iron Man", "Thor", "Hulk", "Black Widow", "Hawkeye"]
avengers[6] = "Spider-Man"
console.log(avengers)
```

if we don't know the last number in the array we can easily add to the end using `.push`
``` javascript
let avengers = ["Captain America", "Iron Man", "Thor", "Hulk", "Black Widow", "Hawkeye"]
avengers.push("Spider-Man")
avengers.push("Ant Man")
console.log(avengers)
```

When creating an array the white space/lines between the array objects doesn't matter, so we can format the array with other lines if we'd like.\
For example the following code runs the exact same as the code above.

``` javascript
let avengers = ["Captain America", 
"Iron Man", 
"Thor", 
"Hulk",         "Black Widow", 
"Hawkeye"]
avengers.push("Spider-Man")
avengers.push("Ant Man")
console.log(avengers)
```

Another method of adding to the end of an array is using indexing and the `.length` of the array
``` javascript
let avengers = ["Captain America", 
"Iron Man", 
"Thor", 
"Hulk",         "Black Widow", 
"Hawkeye"]
avengers.push("Spider-Man")
avengers.push("Ant Man")
let avengersLength = avengers.length;
avengers[avengersLength] = "Black Panther"
console.log(avengers)
```

Now that we have an array we can look at removing items from the array.\
One method is to use the `delete` operator to remove the item if we know the index\
[delete](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/delete)
``` javascript
let avengers = ["Captain America", 
"Iron Man", 
"Thor", 
"Hulk",         "Black Widow", 
"Hawkeye"]
avengers.push("Spider-Man")
avengers.push("Ant Man")
let avengersLength = avengers.length;
avengers[avengersLength] = "Black Panther"
delete avengers[1]
console.log(avengers)
```

This however leaves an empty space in the array, which can cause problems later, so a better way to remove iron man would be using the `.splice`\
[splice](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/splice)

``` javascript
let avengers = ["Captain America", 
"Iron Man", 
"Thor", 
"Hulk",         "Black Widow", 
"Hawkeye"]
avengers.push("Spider-Man")
avengers.push("Ant Man")
let avengersLength = avengers.length;
avengers[avengersLength] = "Black Panther"
//in splice the first 1 is the index of the item we'd like to remove
// the 2nd 1 is the number of elements in the array to remove, we could for instance replace it with 2 to remove iron man, and thor
avengers.splice(1,1)
console.log(avengers)
```

Now that we've gone over a few ways of building the array we cover how to loop and display these items.\
On method we could go in and display each avenger manually from the array with something like

``` javascript
let avengers = ["Captain America", 
"Iron Man", 
"Thor", 
"Hulk",         "Black Widow", 
"Hawkeye"]
avengers.push("Spider-Man")
avengers.push("Ant Man")
let avengersLength = avengers.length;
avengers[avengersLength] = "Black Panther"
console.log(avengers)
document.write("<ul>")
document.write("<li>" + avengers[0] + "</li>")
document.write("<li>" + avengers[1] + "</li>")
document.write("<li>" + avengers[2] + "</li>")
document.write("<li>" + avengers[3] + "</li>")
document.write("<li>" + avengers[4] + "</li>")
```
but if we utilize a `for` loop we can actually display them all without manually writing each `document.write`\
[for loops](https://www.w3schools.com/js/js_loop_for.asp)\
[more loop tutorials](https://www.dofactory.com/tutorial/javascript-loops)
``` javascript
let avengers = ["Captain America", 
"Iron Man", 
"Thor", 
"Hulk",         "Black Widow", 
"Hawkeye"]
avengers.push("Spider-Man")
avengers.push("Ant Man")
let avengersLength = avengers.length;
avengers[avengersLength] = "Black Panther"
console.log(avengers)

document.write("<ul>")
for(let i =0 ; i < avengersLength; i++){
    document.write("<li>" + avengers[i] + "</li>")
}
document.write("</ul>")
```


