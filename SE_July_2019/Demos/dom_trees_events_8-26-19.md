# Nodes, Trees, and Bumble Bee's - 8/26/19
## What is the dom
[What is the html dom](https://www.w3schools.com/whatis/whatis_htmldom.asp)\
![html tree](https://www.w3schools.com/whatis/img_htmltree.gif "html Dom Tree")]

Example of an HTML page and a simple tree
```mermaid
graph TD;
  HTML-->HEAD;
  HTML-->BODY;
  HEAD-->TITLE;
  TITLE-->TitleText[Your Title Here];
  BODY-->CENTER;
  CENTER-->IMG;
  BODY-->HR;
  BODY-->a;
  a-->aText[Link Name];
  BODY-->H1;
  H1-->H1Text[This is a Header];
  BODY-->H2;
  H2-->H2Text[This is a Medium Header];
  BODY-->P;
  P-->pText[This is a new paragraph!];
  BODY-->DIV;
  DIV-->BR;
  DIV-->B;
  B-->I;
  I-->iText[This is a new sentence without a paragraph break, in bold italics.];
  BODY-->HR1[HR]
```

```html
<HTML>
    <HEAD>
        <TITLE>Your Title Here</TITLE>
    </HEAD>
    <BODY BGCOLOR="FFFFFF">
        <CENTER><IMG SRC="clouds.jpg" ALIGN="BOTTOM"> </CENTER>
        <HR>
        <a href="http://somegreatsite.com">Link Name</a>
        is a link to another nifty site

        <H1>This is a Header</H1>

        <H2>This is a Medium Header</H2>
        <P> This is a new paragraph! </P>
        <DIV>
            <BR>
            <B><I>This is a new sentence without a paragraph break, in bold italics.</I>
            </B>
        </DIV>
        <HR>
    </BODY>
</HTML>
```

## Event handlers

[List of browser events](https://developer.mozilla.org/en-US/docs/Web/Events)\
[The HTML Event Listener](https://www.w3schools.com/js/js_htmldom_eventlistener.asp)\
[Event Listener Tutorial](https://developer.mozilla.org/en-US/docs/Learn/JavaScript/Building_blocks/Events)