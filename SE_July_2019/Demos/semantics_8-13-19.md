# Semantics? - 8/13/19
In programming, Semantics refers to the meaning of a piece of code — for example "what effect does running that line of JavaScript have?", or "what purpose or role does that HTML element have" (rather than "what does it look like?".)

[Javascript](https://developer.mozilla.org/en-US/docs/Glossary/Semantics#Semantics_in_JavaScript)\
[CSS](https://developer.mozilla.org/en-US/docs/Glossary/Semantics#Semantics_in_CSS)\
[HTML](https://developer.mozilla.org/en-US/docs/Glossary/Semantics#Semantics_in_HTML)

**[What is title?](https://www.quackit.com/html_5/tags/html_title_tag.cfm)**\
The title is usually displayed in the browser's title bar (at the top). It is also displayed in browser bookmarks and search results.

### [What is html semantics and why should I use them?](https://www.lifewire.com/why-use-semantic-html-3468271)
The benefit of writing semantic HTML stems from what should be the driving goal of any web page — the desire to communicate. By adding semantic tags to your document, you provide additional information about that document, which aids in communication.\
[The very basics of semantics](https://medium.com/adalab/the-importance-of-semantic-html-78e74fb75ff0)\
[Why is semantics important](http://seekbrevity.com/semantic-markup-important-web-design/)\
[full list of HTML elements/semantics](https://developer.mozilla.org/en-US/docs/Web/HTML/Element)



