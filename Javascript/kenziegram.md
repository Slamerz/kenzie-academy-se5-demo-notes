# Kenziegram Demo 
[back to index](./../README.md)
## Part 1

[Node Environment variables](https://medium.com/the-node-js-collection/making-your-node-js-work-everywhere-with-environment-variables-2da8cdf6e786)

### [Express](https://expressjs.com/en/starter/installing.html)

[**Express Middleware**](https://expressjs.com/en/guide/using-middleware.html)\
**built in**\
[static file path](https://expressjs.com/en/starter/static-files.html)\
**Third Party**\
[Multer](https://github.com/expressjs/multer)\
[More Multer documentation](https://github.com/mscdex/busboy#busboy-methods)


## Part 2

[MVC design pattern basics](https://www.sitepoint.com/mvc-design-pattern-javascript/)\
[Using template engines with express](https://expressjs.com/en/guide/using-template-engines.html)


[**Pug**](https://pugjs.org/api/getting-started.html)\
[iterations in Pug](https://pugjs.org/language/iteration.html)\
[Using pug with express](https://gist.github.com/joepie91/c0069ab0e0da40cc7b54b8c2203befe1)


## Part 3
**Parsing response.body in express**\
The following links are documentation to body-parser, but this package has been integrated into express as express.json so it will be very similar if not identical in function\
[body-parser](https://expressjs.com/en/resources/middleware/body-parser.html)\
[Parsing json](https://expressjs.com/en/resources/middleware/body-parser.html#bodyparserjsonoptions)

