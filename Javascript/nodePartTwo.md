# Node.js Part 2

[back to index](./../README.md)\
[Node.js Part 1](nodePartOne.md)

### Global Objects
[**Node Global Objects**](https://nodejs.org/api/globals.html)\
[Using global objects in node](https://stackabuse.com/using-global-variables-in-node-js/)

[**Browser Global Objects**](https://developer.mozilla.org/en-US/docs/Glossary/Global_object)

### Useful node features/modules
#### Built in
[Environment variables](https://www.twilio.com/blog/2017/08/working-with-environment-variables-in-node-js.html)\
[node process](https://nodejs.org/api/process.html)

[**FS**](https://nodejs.org/api/fs.html)\
[Simple Tutorial](https://appdividend.com/2018/10/09/node-js-file-system-module-example-tutorial/)\
[More in depth tutorial](https://www.tutorialspoint.com/nodejs/nodejs_file_system.html)\
[video tutorial](https://www.youtube.com/watch?v=U57kU311-nE)

#### Third Party
[**Electron**](https://electronjs.org/)\
a Cross platform engine using Javascript, HTML, and CSS

[**Cross-fetch**](https://github.com/lquixada/cross-fetch)\
A Fetch API for Node
	
	

### Useful extras
[**Guide to writing/reading Documentation**](http://cassandrawilcox.me/beginners-guide-developer-documentation/)
	
	