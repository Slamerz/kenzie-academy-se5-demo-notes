# Node.js part 1

[back to index](./../README.md)\
[Node.js Part 2](nodePartTwo.md)

## [Node](https://nodejs.org/en/)

##### Npm (Node Package Manager)
[How to install node packages in your project](https://nodesource.com/blog/an-absolute-beginners-guide-to-using-npm/)\
[How to setup a package.json](https://docs.npmjs.com/creating-a-package-json-file)


##### Node Modules
[Documentation](https://nodejs.org/api/modules.html)\
[Quick Modules Tutorial](https://www.w3schools.com/nodejs/nodejs_modules.asp)

	
##### Javascript Deconstruction 	
[A simple deconstruction tutorial](https://wesbos.com/destructuring-objects/)\
[A more detailed look at deconstruction](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Destructuring_assignment)


##### Mentioned npm packages
**fs**\
A package built into node.js used to manage file systems, read, write, delete, etc.\
[Documentation](https://node.readthedocs.io/en/latest/api/fs/)\
[simple example scripts](https://www.w3schools.com/nodejs/nodejs_filesystem.asp)\

**Axios**\
Promise based HTTP client for the browser and node.js\
[Website](https://github.com/axios/axios)\
[Simple Tutorial](https://appdividend.com/2018/08/30/getting-started-with-axios-tutorial-example/#2_Create_the_serverjs_file)\
[More Complicated Tutorial](https://medium.com/codingthesmartway-com-blog/getting-started-with-axios-166cb0035237)
		
**Node-Fetch**\
A light-weight module that emulates the functions of window.fetch in Node.JS \
[Website](https://www.npmjs.com/package/node-fetch)

	
## Useful Visual Studio Code Extensions:
**[Node.js Extension Pack](https://marketplace.visualstudio.com/items?itemName=waderyan.nodejs-extension-pack)**\
Helps with NPM, package pathing and a few other very useful features/extensions.

**[Code Runner](https://marketplace.visualstudio.com/items?itemName=formulahendry.code-runner)**\
Makes running files and code snippets very quick and easy.


### Useful packages/tools for testing your code in Node.js
#### npm packages
**[mocha](https://mochajs.org/)**\
Used for writing/running unit tests on functions.\
[Tutorial](https://codeburst.io/how-to-test-javascript-with-mocha-the-basics-80132324752e)\
Visual Studio Code Extension Support: [Mocha Sidebar](https://marketplace.visualstudio.com/items?itemName=maty.vscode-mocha-sidebar) || [Mocha Test Explorer](https://marketplace.visualstudio.com/items?itemName=hbenl.vscode-mocha-test-adapter)

**[Chai](https://www.chaijs.com/)**\
Extends mocha making the unit tests easier to write, and read unit tests.\
[Tutorial](https://codeburst.io/javascript-unit-testing-using-mocha-and-chai-1d97d9f18e71)
	
	